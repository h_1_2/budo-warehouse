package org.budo.warehouse.logic.api;

/**
 * @author limingwei
 */
public interface DataEntry {
    Integer getSourceDataNodeId();

    String getEventType();

    String getSchemaName();

    String getTableName();

    /**
     * 行数
     */
    Integer getRowCount();

    /**
     * 列数
     */
    Integer getColumnCount(Integer rowIndex);

    /**
     * 列名
     */
    String getColumnName(Integer rowIndex, Integer columnIndex);

    /**
     * 列是否主键
     */
    Boolean getColumnIsKey(Integer rowIndex, Integer columnIndex);

    /**
     * 变更前的值
     */
    String getColumnValueBefore(Integer rowIndex, Integer columnIndex);

    /**
     * 变更后的值
     */
    String getColumnValueAfter(Integer rowIndex, Integer columnIndex);
}