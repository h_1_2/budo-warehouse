package org.budo.warehouse.logic.consumer.jdbc;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.budo.graph.annotation.SpringGraph;
import org.budo.support.javax.sql.util.JdbcUtil;
import org.budo.warehouse.logic.api.DataEntry;
import org.budo.warehouse.logic.bean.DynamicBeanService;

import lombok.Getter;
import lombok.Setter;

/**
 * @author limingwei
 */
@Getter
@Setter
public class JdbcDataConsumer extends AbstractJdbcDataConsumer {
    @Resource
    private DynamicBeanService dynamicBeanService;

    @SpringGraph
    @Override
    public void consume(DataEntry dataEntry) {
        List<SqlUnit> sqlUnits = this.buildSql(dataEntry);
        if (null == sqlUnits) {
            return;
        }

        DataSource dataSource = dynamicBeanService.dataSource(this.getDataNode());

        for (SqlUnit sqlUnit : sqlUnits) {
            JdbcUtil.executeUpdate(dataSource, sqlUnit.getSql(), sqlUnit.getParameters());
        }
    }
}